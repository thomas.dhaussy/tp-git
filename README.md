# PROJET : TP GIT

## Auteurs :
* Thomas Dhaussy
* Louis Glandieres

## Objectifs du TP :
### Apprendre à utiliser Git et Gitlab dans le but de savoir rendre nos TP grâce à ces outils

## Description du projet
Notre projet contient 5 fichiers textes :
* fruits.txt
* legumes.txt
* sauces.txt
* epices.txt
* herbes.txt

A ces fichiers ont été ajoutés des éléments depuis le terminal.

Certains fichiers ont été créés et ajoutés à des branches qui ont ensuite été *merge* à la branche master.
Enfin le tout a été envoyé vers un dépôt distant par les 2 élèves du binôme en simultanée, parfois sans conflits, parfois avec.
